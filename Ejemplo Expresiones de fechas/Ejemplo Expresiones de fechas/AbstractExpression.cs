﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo_Expresiones_de_fechas
{
    interface AbstractExpression
    {
        void Evaluate(Context context);
    }
}
