﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo_Expresiones_de_fechas
{
    public class MonthExpression : AbstractExpression
    {
         void AbstractExpression.Evaluate(Context context)
        {
            string expression = context.expression;
            context.expression = expression.Replace("MM", context.date.Month.ToString());
        }
    }
}
